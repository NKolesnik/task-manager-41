package ru.t1consulting.nkolesnik.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1consulting.nkolesnik.tm.api.service.*;
import ru.t1consulting.nkolesnik.tm.enumerated.Status;
import ru.t1consulting.nkolesnik.tm.exception.field.ProjectIdEmptyException;
import ru.t1consulting.nkolesnik.tm.exception.field.TaskIdEmptyException;
import ru.t1consulting.nkolesnik.tm.exception.field.UserIdEmptyException;
import ru.t1consulting.nkolesnik.tm.marker.DataCategory;
import ru.t1consulting.nkolesnik.tm.dto.model.ProjectDTO;
import ru.t1consulting.nkolesnik.tm.dto.model.TaskDTO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

@Category(DataCategory.class)
public class ProjectTaskServiceTest {

    @NotNull
    private static final String TASK_NAME_PREFIX = "TEST_TASK_NAME";

    @NotNull
    private static final String TASK_DESCRIPTION_PREFIX = "TEST_TASK_DESCRIPTION";

    @NotNull
    private static final String PROJECT_NAME_PREFIX = "TEST_PROJECT_NAME";

    @NotNull
    private static final String PROJECT_DESCRIPTION_PREFIX = "TEST_PROJECT_DESCRIPTION";

    @Nullable
    private static final String NULL_USER_ID = null;

    @Nullable
    private static final String NULL_TASK_ID = null;

    @Nullable
    private static final String NULL_PROJECT_ID = null;
    @NotNull
    private static final IPropertyService propertyService = new PropertyService();
    @NotNull
    private static String userId = "";
    @NotNull
    private static String projectId = "";
    @NotNull
    private static String taskId = "";
    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final IProjectService projectService = new ProjectService(connectionService);

    @NotNull
    private final ITaskService taskService = new TaskService(connectionService);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectService, taskService);

    @NotNull
    private TaskDTO task;

    @NotNull
    private ProjectDTO project;

    @BeforeClass
    public static void prepareTestEnvironment() {
        getTestData();
        saveDbBackup();
    }

    @AfterClass
    public static void restoreProdEnvironment() {
        loadDbBackup();
    }

    @SneakyThrows
    public static void saveDbBackup() {
        @NotNull final String userName = propertyService.getDatabaseUsername();
        @NotNull final String userPassword = propertyService.getDatabasePassword();
        @NotNull final String url = propertyService.getDatabaseConnectionString();
        @NotNull final Connection connection = DriverManager.getConnection(url, userName, userPassword);
        connection.setAutoCommit(false);
        @NotNull final String sqlSaveProjectData = "SELECT * INTO \"backup_projects\" FROM \"projects\";";
        @NotNull final String sqlClearProjectTable = "DELETE FROM \"projects\";";
        @NotNull final String sqlSaveTaskData = "SELECT * INTO \"backup_tasks\" FROM \"tasks\";";
        @NotNull final String sqlClearTaskTable = "DELETE FROM \"tasks\";";
        try {
            Statement statement = connection.createStatement();
            statement.executeUpdate(sqlSaveProjectData);
            statement.executeUpdate(sqlSaveTaskData);
            statement.executeUpdate(sqlClearTaskTable);
            statement.executeUpdate(sqlClearProjectTable);
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    public static void loadDbBackup() {
        @NotNull final String userName = propertyService.getDatabaseUsername();
        @NotNull final String userPassword = propertyService.getDatabasePassword();
        @NotNull final String url = propertyService.getDatabaseConnectionString();
        @NotNull final Connection connection = DriverManager.getConnection(url, userName, userPassword);
        connection.setAutoCommit(false);
        @NotNull final String sqlClearProjectTable = "DELETE FROM \"projects\";";
        @NotNull final String sqlLoadProjectData = "INSERT INTO \"projects\" (SELECT * FROM \"backup_projects\");";
        @NotNull final String sqlDropBackupProjectTable = "DROP TABLE \"backup_projects\";";
        @NotNull final String sqlClearTaskTable = "DELETE FROM \"tasks\";";
        @NotNull final String sqlLoadTaskData = "INSERT INTO \"tasks\" (SELECT * FROM \"backup_tasks\");";
        @NotNull final String sqlDropBackupTaskTable = "DROP TABLE \"backup_tasks\";";
        try {
            Statement statement = connection.createStatement();
            statement.executeUpdate(sqlClearTaskTable);
            statement.executeUpdate(sqlClearProjectTable);
            statement.executeUpdate(sqlLoadProjectData);
            statement.executeUpdate(sqlLoadTaskData);
            statement.executeUpdate(sqlDropBackupTaskTable);
            statement.executeUpdate(sqlDropBackupProjectTable);
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    public static void getTestData() {
        @NotNull final String userName = propertyService.getDatabaseUsername();
        @NotNull final String userPassword = propertyService.getDatabasePassword();
        @NotNull final String url = propertyService.getDatabaseConnectionString();
        @NotNull final Connection connection = DriverManager.getConnection(url, userName, userPassword);
        connection.setAutoCommit(false);
        String sqlGetTestUserIdData = "SELECT id FROM \"users\" WHERE \"login\" ='test';";
        try {
            Statement statement = connection.createStatement();
            @NotNull final ResultSet userSet = statement.executeQuery(sqlGetTestUserIdData);
            if(userSet.next()){
                userId = userSet.getString(1);
            }
            String sqlGetTestProjectIdData = "SELECT id FROM \"projects\" WHERE \"user_id\" = '"+userId+"';";
            @NotNull final ResultSet projectSet = statement.executeQuery(sqlGetTestProjectIdData);
            if(projectSet.next()){
                projectId = projectSet.getString(1);
            }
            String sqlGetTestTaskIdData = "SELECT id FROM \"tasks\" WHERE \"user_id\" = '"+userId+"' " +
                    "AND \"project_id\" = '"+projectId+"';";
            @NotNull final ResultSet taskSet = statement.executeQuery(sqlGetTestTaskIdData);
            if(taskSet.next()){
                taskId = taskSet.getString(1);
            }
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Before
    public void setup() {
        task = createOneTask();
        project = createOneProject();
    }

    @After
    public void cleanup() {
        taskService.clear();
        projectService.clear();
    }

    @Test
    public void bindTaskToProject() {
        projectService.add(userId, project);
        taskService.add(userId, task);
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> projectTaskService.bindTaskToProject(NULL_USER_ID, taskId, projectId)
        );
        Assert.assertThrows(
                TaskIdEmptyException.class,
                () -> projectTaskService.bindTaskToProject(userId, projectId, NULL_TASK_ID)
        );
        Assert.assertThrows(
                ProjectIdEmptyException.class,
                () -> projectTaskService.bindTaskToProject(userId, NULL_PROJECT_ID, taskId)
        );
        projectTaskService.bindTaskToProject(userId, projectId, taskId);
        @Nullable final TaskDTO repositoryTask = taskService.findById(taskId);
        Assert.assertNotNull(repositoryTask);
        Assert.assertEquals(projectId, repositoryTask.getProjectId());
    }

    @Test
    public void unbindTaskToProject() {
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> projectTaskService.unbindTaskFromProject(NULL_USER_ID, taskId, projectId)
        );
        Assert.assertThrows(
                TaskIdEmptyException.class,
                () -> projectTaskService.unbindTaskFromProject(userId, projectId, NULL_TASK_ID)
        );
        Assert.assertThrows(
                ProjectIdEmptyException.class,
                () -> projectTaskService.unbindTaskFromProject(userId, NULL_PROJECT_ID, taskId)
        );
        projectService.add(userId, project);
        taskService.add(userId, task);
        projectTaskService.bindTaskToProject(userId, projectId, taskId);
        @Nullable TaskDTO repositoryTask = taskService.findById(taskId);
        Assert.assertNotNull(repositoryTask);
        Assert.assertEquals(projectId, repositoryTask.getProjectId());
        projectTaskService.unbindTaskFromProject(userId, projectId, taskId);
        repositoryTask = taskService.findById(taskId);
        Assert.assertNotNull(repositoryTask);
        Assert.assertNull(repositoryTask.getProjectId());
    }

    @NotNull
    private ProjectDTO createOneProject() {
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setId(projectId);
        project.setUserId(userId);
        project.setName(PROJECT_NAME_PREFIX);
        project.setDescription(PROJECT_DESCRIPTION_PREFIX);
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @NotNull
    private TaskDTO createOneTask() {
        @NotNull final TaskDTO task = new TaskDTO();
        task.setId(taskId);
        task.setUserId(userId);
        task.setName(TASK_NAME_PREFIX);
        task.setDescription(TASK_DESCRIPTION_PREFIX);
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

}
