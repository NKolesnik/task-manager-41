package ru.t1consulting.nkolesnik.tm.repository;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.mybatis.dynamic.sql.SqlBuilder;
import org.mybatis.dynamic.sql.render.RenderingStrategy;
import org.mybatis.dynamic.sql.update.render.UpdateStatementProvider;
import ru.t1consulting.nkolesnik.tm.api.repository.IUserRepository;
import ru.t1consulting.nkolesnik.tm.api.service.IConnectionService;
import ru.t1consulting.nkolesnik.tm.api.service.IPropertyService;
import ru.t1consulting.nkolesnik.tm.enumerated.Role;
import ru.t1consulting.nkolesnik.tm.marker.DataCategory;
import ru.t1consulting.nkolesnik.tm.dto.model.UserDTO;
import ru.t1consulting.nkolesnik.tm.model.UserProvider;
import ru.t1consulting.nkolesnik.tm.service.ConnectionService;
import ru.t1consulting.nkolesnik.tm.service.PropertyService;
import ru.t1consulting.nkolesnik.tm.util.HashUtil;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.mybatis.dynamic.sql.SqlBuilder.isEqualTo;
import static ru.t1consulting.nkolesnik.tm.model.UserProvider.*;

@Category(DataCategory.class)
public class UserRepositoryTest {

    @NotNull
    private static final String USER_LOGIN_PREFIX = "TEST_USER_LOGIN";

    @NotNull
    private static final String USER_EMAIL_PREFIX = "TEST_USER_@EMAIL";

    @NotNull
    private static final String USER_PASSWORD_PREFIX = "TEST_USER_PASSWORD";

    @NotNull
    private static final String USER_PASSWORD_SECRET = "123654789";

    @NotNull
    private static final Integer USER_PASSWORD_ITERATION = 3;

    private static final long REPOSITORY_SIZE = 100L;

    private static final long EMPTY_REPOSITORY_SIZE = 0L;

    @Nullable
    private static final String NULL_USER_ID = null;

    @Nullable
    private static final UserDTO NULL_USER = null;

    @Nullable
    private static final String NULL_USER_LOGIN = null;

    @NotNull
    private static final String EMPTY_USER_LOGIN = "";

    @NotNull
    private static final String BAD_USER_LOGIN = UUID.randomUUID().toString();

    @NotNull
    private static final String EMPTY_USER_ID = "";

    @NotNull
    private static final String BAD_USER_ID = UUID.randomUUID().toString();

    @NotNull
    private static final String EMPTY_USER_EMAIL = "";

    @Nullable
    private static final String NULL_USER_EMAIL = null;

    @NotNull
    private static final String BAD_USER_EMAIL = "";

    @NotNull
    private static String TEST_USER_ID;

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private UserDTO user;

    @NotNull
    private List<UserDTO> userList;

    @BeforeClass
    public static void prepareTestEnvironment() {
        getTestData();
        saveDbBackup();
    }

    @AfterClass
    public static void restoreProdEnvironment() {
        loadDbBackup();
    }

    @SneakyThrows
    public static void saveDbBackup() {
        @NotNull final String userName = propertyService.getDatabaseUsername();
        @NotNull final String userPassword = propertyService.getDatabasePassword();
        @NotNull final String url = propertyService.getDatabaseConnectionString();
        @NotNull final Connection connection = DriverManager.getConnection(url, userName, userPassword);
        connection.setAutoCommit(false);
        @NotNull final String sqlSaveUserData = "SELECT * INTO \"backup_users\" FROM \"users\";";
        @NotNull final String sqlClearUserTable = "DELETE FROM \"users\";";
        @NotNull final String sqlSaveProjectData = "SELECT * INTO \"backup_projects\" FROM \"projects\";";
        @NotNull final String sqlClearProjectTable = "DELETE FROM \"projects\";";
        @NotNull final String sqlSaveTaskData = "SELECT * INTO \"backup_tasks\" FROM \"tasks\";";
        @NotNull final String sqlClearTaskTable = "DELETE FROM \"tasks\";";
        try {
            Statement statement = connection.createStatement();
            statement.executeUpdate(sqlSaveUserData);
            statement.executeUpdate(sqlSaveProjectData);
            statement.executeUpdate(sqlSaveTaskData);
            statement.executeUpdate(sqlClearTaskTable);
            statement.executeUpdate(sqlClearProjectTable);
            statement.executeUpdate(sqlClearUserTable);
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    public static void loadDbBackup() {
        @NotNull final String userName = propertyService.getDatabaseUsername();
        @NotNull final String userPassword = propertyService.getDatabasePassword();
        @NotNull final String url = propertyService.getDatabaseConnectionString();
        @NotNull final Connection connection = DriverManager.getConnection(url, userName, userPassword);
        connection.setAutoCommit(false);
        @NotNull final String sqlClearUserTable = "DELETE FROM \"users\";";
        @NotNull final String sqlLoadUserData = "INSERT INTO \"users\" (SELECT * FROM \"backup_users\");";
        @NotNull final String sqlDropBackupUserTable = "DROP TABLE \"backup_users\";";
        @NotNull final String sqlClearProjectTable = "DELETE FROM \"projects\";";
        @NotNull final String sqlLoadProjectData = "INSERT INTO \"projects\" (SELECT * FROM \"backup_projects\");";
        @NotNull final String sqlDropBackupProjectTable = "DROP TABLE \"backup_projects\";";
        @NotNull final String sqlClearTaskTable = "DELETE FROM \"tasks\";";
        @NotNull final String sqlLoadTaskData = "INSERT INTO \"tasks\" (SELECT * FROM \"backup_tasks\");";
        @NotNull final String sqlDropBackupTaskTable = "DROP TABLE \"backup_tasks\";";
        try {
            Statement statement = connection.createStatement();
            statement.executeUpdate(sqlClearUserTable);
            statement.executeUpdate(sqlClearTaskTable);
            statement.executeUpdate(sqlClearProjectTable);
            statement.executeUpdate(sqlLoadUserData);
            statement.executeUpdate(sqlLoadProjectData);
            statement.executeUpdate(sqlLoadTaskData);
            statement.executeUpdate(sqlDropBackupTaskTable);
            statement.executeUpdate(sqlDropBackupProjectTable);
            statement.executeUpdate(sqlDropBackupUserTable);
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    public static void getTestData() {
        @NotNull final String userName = propertyService.getDatabaseUsername();
        @NotNull final String userPassword = propertyService.getDatabasePassword();
        @NotNull final String url = propertyService.getDatabaseConnectionString();
        @NotNull final Connection connection = DriverManager.getConnection(url, userName, userPassword);
        connection.setAutoCommit(false);
        String sqlGetTestUserIdData = "SELECT id FROM \"users\" WHERE \"login\" ='test';";
        try {
            Statement statement = connection.createStatement();
            @NotNull final ResultSet userSet = statement.executeQuery(sqlGetTestUserIdData);
            if(userSet.next()){
                TEST_USER_ID = userSet.getString(1);
            }
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Before
    public void setup() {
        user = createUser();
        userList = createManyUsers();
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IUserRepository repository = session.getMapper(IUserRepository.class);
            repository.clear();
        }
    }

    @After
    public void cleanup() {
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IUserRepository repository = session.getMapper(IUserRepository.class);
            repository.clear();
        }
    }

    @Test
    public void add() {
        @NotNull final SqlSession session = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository repository = session.getMapper(IUserRepository.class);
            Assert.assertThrows(Exception.class, () -> repository.add(NULL_USER));
            session.rollback();
            @NotNull final UserDTO user = new UserDTO();
            repository.clear();
            repository.add(user);
            Assert.assertEquals(1, repository.getSize());
            session.commit();
            repository.clear();
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Test
    public void getSize() {
        @NotNull final SqlSession session = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository repository = session.getMapper(IUserRepository.class);
            Assert.assertEquals(EMPTY_REPOSITORY_SIZE, repository.getSize());
            session.commit();
            for (UserDTO user : userList)
                repository.add(user);
            Assert.assertEquals(REPOSITORY_SIZE, repository.getSize());
            session.commit();
            repository.clear();
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Test
    public void findAll() {
        @NotNull final SqlSession session = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository repository = session.getMapper(IUserRepository.class);
            for (UserDTO user : userList)
                repository.add(user);
            @Nullable List<UserDTO> repositoryUsers = repository.findAll();
            Assert.assertNotNull(repositoryUsers);
            Assert.assertEquals(REPOSITORY_SIZE, repositoryUsers.size());
            session.commit();
            repository.clear();
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Test
    public void findById() {
        @NotNull final SqlSession session = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository repository = session.getMapper(IUserRepository.class);
            repository.add(user);
            Assert.assertNull(repository.findById(NULL_USER_ID));
            session.commit();
            Assert.assertNull(repository.findById(BAD_USER_ID));
            session.commit();
            Assert.assertNull(repository.findById(EMPTY_USER_ID));
            session.commit();
            @Nullable UserDTO repositoryUser = repository.findById(TEST_USER_ID);
            Assert.assertNotNull(repositoryUser);
            Assert.assertEquals(TEST_USER_ID, repositoryUser.getId());
            session.commit();
            repository.clear();
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Test
    public void findByLogin() {
        @NotNull final SqlSession session = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository repository = session.getMapper(IUserRepository.class);
            repository.add(user);
            Assert.assertNull(repository.findByLogin(NULL_USER_LOGIN));
            session.commit();
            Assert.assertNull(repository.findByLogin(BAD_USER_LOGIN));
            session.commit();
            Assert.assertNull(repository.findByLogin(EMPTY_USER_LOGIN));
            session.commit();
            @Nullable UserDTO repositoryUser = repository.findByLogin(USER_LOGIN_PREFIX);
            Assert.assertNotNull(repositoryUser);
            Assert.assertEquals(TEST_USER_ID, repositoryUser.getId());
            session.commit();
            repository.clear();
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Test
    public void findByEmail() {
        @NotNull final SqlSession session = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository repository = session.getMapper(IUserRepository.class);
            repository.add(user);
            Assert.assertNull(repository.findByEmail(NULL_USER_EMAIL));
            session.commit();
            Assert.assertNull(repository.findByEmail(BAD_USER_EMAIL));
            session.commit();
            Assert.assertNull(repository.findByEmail(EMPTY_USER_EMAIL));
            session.commit();
            @Nullable UserDTO repositoryUser = repository.findByEmail(USER_EMAIL_PREFIX);
            Assert.assertNotNull(repositoryUser);
            Assert.assertEquals(TEST_USER_ID, repositoryUser.getId());
            session.commit();
            repository.clear();
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Test
    public void isLoginExist() {
        @NotNull final SqlSession session = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository repository = session.getMapper(IUserRepository.class);
            repository.add(user);
            Assert.assertFalse(repository.isLoginExist(NULL_USER_LOGIN));
            session.commit();
            Assert.assertFalse(repository.isLoginExist(BAD_USER_LOGIN));
            session.commit();
            Assert.assertFalse(repository.isLoginExist(EMPTY_USER_LOGIN));
            session.commit();
            Assert.assertTrue(repository.isLoginExist(USER_LOGIN_PREFIX));
            session.commit();
            repository.clear();
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Test
    public void isEmailExist() {
        @NotNull final SqlSession session = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository repository = session.getMapper(IUserRepository.class);
            repository.add(user);
            Assert.assertFalse(repository.isEmailExist(NULL_USER_EMAIL));
            session.commit();
            Assert.assertFalse(repository.isEmailExist(BAD_USER_EMAIL));
            session.commit();
            Assert.assertFalse(repository.isEmailExist(EMPTY_USER_EMAIL));
            session.commit();
            Assert.assertTrue(repository.isEmailExist(USER_EMAIL_PREFIX));
            session.commit();
            repository.clear();
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Test
    public void lockUserByLogin() {
        @NotNull final SqlSession session = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository repository = session.getMapper(IUserRepository.class);
            repository.add(user);
            @Nullable UserDTO repositoryUser = repository.findByLogin(USER_LOGIN_PREFIX);
            Assert.assertNotNull(repositoryUser);
            Assert.assertEquals(user.getLocked(), repositoryUser.getLocked());
            repository.lockUserByLogin(USER_LOGIN_PREFIX);
            session.commit();
            Assert.assertTrue(repository.findByLogin(USER_LOGIN_PREFIX).getLocked());
            session.commit();
            repository.clear();
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Test
    public void unlockUserByLogin() {
        @NotNull final SqlSession session = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository repository = session.getMapper(IUserRepository.class);
            repository.add(user);
            @Nullable UserDTO repositoryUser = repository.findByLogin(USER_LOGIN_PREFIX);
            Assert.assertNotNull(repositoryUser);
            repository.lockUserByLogin(USER_LOGIN_PREFIX);
            session.commit();
            Assert.assertTrue(repository.findByLogin(USER_LOGIN_PREFIX).getLocked());
            repository.unlockUserByLogin(USER_LOGIN_PREFIX);
            session.commit();
            Assert.assertFalse(repository.findByLogin(USER_LOGIN_PREFIX).getLocked());
            session.commit();
            repository.clear();
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Test
    public void update() {
        @NotNull final SqlSession session = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository repository = session.getMapper(IUserRepository.class);
            repository.add(user);
            session.commit();
            @NotNull final String newName = "NEW NAME";
            @NotNull final Boolean newLocked = true;
            @NotNull final Role newRole = Role.ADMIN;
            @NotNull final String oldName = user.getFirstName();
            @NotNull final Boolean oldLocked = user.getLocked();
            @NotNull final Role oldRole = user.getRole();
            user.setFirstName(newName);
            user.setLocked(newLocked);
            user.setRole(newRole);
            @NotNull final UpdateStatementProvider statement = SqlBuilder.update(users).
                    set(login).equalTo(user.getLogin()).
                    set(passwordHash).equalTo(user.getPasswordHash()).
                    set(email).equalTo(user.getEmail()).
                    set(firstName).equalTo(user.getFirstName()).
                    set(middleName).equalTo(user.getMiddleName()).
                    set(lastName).equalTo(user.getLastName()).
                    set(locked).equalTo(user.getLocked()).
                    set(role).equalTo(user.getRole().name()).
                    where(UserProvider.id, isEqualTo(user.getId())).
                    build().render(RenderingStrategy.MYBATIS3);
            repository.update(statement);
            session.commit();
            @Nullable final UserDTO repositoryUser = repository.findByLogin(USER_LOGIN_PREFIX);
            Assert.assertNotNull(repositoryUser);
            Assert.assertEquals(newName, repositoryUser.getFirstName());
            Assert.assertEquals(newLocked, repositoryUser.getLocked());
            Assert.assertEquals(newRole, repositoryUser.getRole());
            session.commit();
            user.setFirstName(oldName);
            user.setRole(oldRole);
            user.setLocked(oldLocked);
            repository.update(statement);
            session.commit();
            repository.clear();
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Test
    public void clear() {
        @NotNull final SqlSession session = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository repository = session.getMapper(IUserRepository.class);
            for (UserDTO user : userList)
                repository.add(user);
            @Nullable List<UserDTO> repositoryUsers = repository.findAll();
            Assert.assertNotNull(repositoryUsers);
            Assert.assertEquals(REPOSITORY_SIZE, repositoryUsers.size());
            session.commit();
            repository.clear();
            repositoryUsers = repository.findAll();
            session.commit();
            Assert.assertEquals(EMPTY_REPOSITORY_SIZE, repositoryUsers.size());
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Test
    public void remove() {
        @NotNull final SqlSession session = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository repository = session.getMapper(IUserRepository.class);
            repository.add(user);
            @Nullable UserDTO repositoryUser = repository.findByLogin(USER_LOGIN_PREFIX);
            Assert.assertNotNull(repositoryUser);
            Assert.assertEquals(USER_LOGIN_PREFIX, repositoryUser.getLogin());
            session.commit();
            repository.remove(user);
            session.commit();
            Assert.assertNull(repository.findByLogin(USER_LOGIN_PREFIX));
            session.commit();
            repository.clear();
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Test
    public void removeByLogin() {
        @NotNull final SqlSession session = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository repository = session.getMapper(IUserRepository.class);
            repository.add(user);
            @Nullable UserDTO repositoryUser = repository.findByLogin(USER_LOGIN_PREFIX);
            Assert.assertNotNull(repositoryUser);
            Assert.assertEquals(USER_LOGIN_PREFIX, repositoryUser.getLogin());
            session.commit();
            repository.removeByLogin(USER_LOGIN_PREFIX);
            session.commit();
            Assert.assertNull(repository.findByLogin(USER_LOGIN_PREFIX));
            session.commit();
            repository.clear();
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Test
    public void removeById() {
        @NotNull final SqlSession session = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository repository = session.getMapper(IUserRepository.class);
            repository.add(user);
            @Nullable UserDTO repositoryUser = repository.findByLogin(USER_LOGIN_PREFIX);
            Assert.assertNotNull(repositoryUser);
            Assert.assertEquals(USER_LOGIN_PREFIX, repositoryUser.getLogin());
            session.commit();
            repository.removeById(TEST_USER_ID);
            session.commit();
            Assert.assertNull(repository.findByLogin(USER_LOGIN_PREFIX));
            session.commit();
            repository.clear();
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @NotNull
    private List<UserDTO> createManyUsers() {
        @NotNull final List<UserDTO> users = new ArrayList<>();
        for (int i = 0; i < REPOSITORY_SIZE; i++) {
            @NotNull final UserDTO user = createUser(USER_LOGIN_PREFIX + i, HashUtil.salt(
                    USER_PASSWORD_PREFIX + i,
                    USER_PASSWORD_SECRET + i,
                    USER_PASSWORD_ITERATION + i)
            );
            user.setEmail(USER_EMAIL_PREFIX + i);
            users.add(user);
        }
        return users;
    }

    @NotNull
    private UserDTO createUser() {
        @NotNull final UserDTO user = new UserDTO();
        user.setId(TEST_USER_ID);
        user.setLogin(USER_LOGIN_PREFIX);
        user.setPasswordHash(HashUtil.salt(USER_PASSWORD_PREFIX, USER_PASSWORD_SECRET, USER_PASSWORD_ITERATION));
        user.setRole(Role.ADMIN);
        user.setEmail(USER_EMAIL_PREFIX);
        return user;
    }

    @NotNull
    private UserDTO createUser(@NotNull final String name, @NotNull final String password) {
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(name);
        user.setPasswordHash(HashUtil.salt(password, USER_PASSWORD_SECRET, USER_PASSWORD_ITERATION));
        user.setRole(Role.ADMIN);
        user.setEmail(USER_EMAIL_PREFIX);
        return user;
    }

}
