package ru.t1consulting.nkolesnik.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.t1consulting.nkolesnik.tm.api.repository.IUserOwnedRepository;
import ru.t1consulting.nkolesnik.tm.api.service.IConnectionService;
import ru.t1consulting.nkolesnik.tm.api.service.IUserOwnedService;
import ru.t1consulting.nkolesnik.tm.dto.model.AbstractUserOwnedModelDTO;

public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModelDTO, R extends IUserOwnedRepository<M>>
        extends AbstractService<M, R> implements IUserOwnedService<M> {

    public AbstractUserOwnedService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

}
