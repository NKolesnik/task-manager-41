package ru.t1consulting.nkolesnik.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.mybatis.dynamic.sql.update.render.UpdateStatementProvider;
import org.mybatis.dynamic.sql.util.SqlProviderAdapter;
import ru.t1consulting.nkolesnik.tm.dto.model.UserDTO;

import java.util.List;

public interface IUserRepository extends IRepository<UserDTO> {

    @Insert("INSERT INTO users(id, login, password_hash, email, fst_name, mid_name, last_name, role, locked_flg)" +
            "VALUES (#{id}, #{login}, #{passwordHash}, #{email}, " +
            "#{firstName}, #{middleName}, #{lastName}, #{role}, #{locked});")
    void add(@Nullable UserDTO user);

    @Select("SELECT COUNT(1) FROM users;")
    long getSize();

    @NotNull
    @Select("SELECT id, login, password_hash, email, fst_name, mid_name, last_name, role, locked_flg " +
            "FROM users;")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "fst_name"),
            @Result(property = "middleName", column = "mid_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "locked", column = "locked_flg")
    })
    List<UserDTO> findAll();

    @Nullable
    @Select("SELECT id, login, password_hash, email, fst_name, mid_name, last_name, role, locked_flg " +
            "FROM users WHERE id = #{id};")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "fst_name"),
            @Result(property = "middleName", column = "mid_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "locked", column = "locked_flg")
    })
    UserDTO findById(@Nullable String id);

    @Nullable
    @Select("SELECT id, login, password_hash, email, fst_name, mid_name, last_name, role, locked_flg " +
            "FROM users WHERE login = #{login}")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "fst_name"),
            @Result(property = "middleName", column = "mid_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "locked", column = "locked_flg")
    })
    UserDTO findByLogin(@Nullable String login);

    @Nullable
    @Select("SELECT id, login, password_hash, email, fst_name, mid_name, last_name, role, locked_flg " +
            "FROM users WHERE email = #{email}")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "fst_name"),
            @Result(property = "middleName", column = "mid_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "locked", column = "locked_flg")
    })
    UserDTO findByEmail(@Nullable String email);

    @NotNull
    @Select("SELECT COUNT(1) = 1 FROM users WHERE login = #{login}")
    Boolean isLoginExist(@Nullable String login);

    @NotNull
    @Select("SELECT COUNT(1) = 1 FROM users WHERE email = #{email}")
    Boolean isEmailExist(@Nullable String email);

    @Update("UPDATE users SET locked_flg = 'true' WHERE login = #{login};")
    void lockUserByLogin(@Nullable String login);

    @Update("UPDATE users SET locked_flg = 'false' WHERE login = #{login};")
    void unlockUserByLogin(@Nullable String login);

    @Update("UPDATE users SET fst_name = #{firstName}, mid_name = #{middleName}, last_name = #{lastName} " +
            "WHERE id = #{id};")
    void updateUser(@Param("id") @Nullable String id,
                    @Param("firstName") @Nullable String firstName,
                    @Param("middleName") @Nullable String middleName,
                    @Param("lastName") @Nullable String lastName);

    @UpdateProvider(type = SqlProviderAdapter.class, method = "update")
    void setPassword(@NotNull UpdateStatementProvider statement);

    @UpdateProvider(type = SqlProviderAdapter.class, method = "update")
    void update(@NotNull UpdateStatementProvider statement);

    @Delete("DELETE FROM users;")
    void clear();

    @Delete("DELETE FROM users WHERE id = #{id};")
    void remove(@Nullable UserDTO user);

    @Delete("DELETE FROM users WHERE login = #{login};")
    void removeByLogin(@Nullable String login);

    @Delete("DELETE FROM users WHERE id = #{id};")
    void removeById(@Nullable String id);

}
