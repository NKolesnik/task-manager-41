package ru.t1consulting.nkolesnik.tm.api.service;

import ru.t1consulting.nkolesnik.tm.dto.model.AbstractUserOwnedModelDTO;

public interface IUserOwnedService<M extends AbstractUserOwnedModelDTO> extends IService<M> {

}
