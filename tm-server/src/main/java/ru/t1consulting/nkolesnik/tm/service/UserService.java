package ru.t1consulting.nkolesnik.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.mybatis.dynamic.sql.SqlBuilder;
import org.mybatis.dynamic.sql.render.RenderingStrategy;
import org.mybatis.dynamic.sql.update.render.UpdateStatementProvider;
import ru.t1consulting.nkolesnik.tm.api.repository.IProjectRepository;
import ru.t1consulting.nkolesnik.tm.api.repository.ITaskRepository;
import ru.t1consulting.nkolesnik.tm.api.repository.IUserRepository;
import ru.t1consulting.nkolesnik.tm.api.service.IConnectionService;
import ru.t1consulting.nkolesnik.tm.api.service.IPropertyService;
import ru.t1consulting.nkolesnik.tm.api.service.IUserService;
import ru.t1consulting.nkolesnik.tm.enumerated.Role;
import ru.t1consulting.nkolesnik.tm.exception.entity.UserNotFoundException;
import ru.t1consulting.nkolesnik.tm.exception.field.*;
import ru.t1consulting.nkolesnik.tm.dto.model.UserDTO;
import ru.t1consulting.nkolesnik.tm.model.UserProvider;
import ru.t1consulting.nkolesnik.tm.util.HashUtil;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static org.mybatis.dynamic.sql.SqlBuilder.isEqualTo;
import static ru.t1consulting.nkolesnik.tm.model.UserProvider.*;

public final class UserService extends AbstractService<UserDTO, IUserRepository> implements IUserService {

    @NotNull
    private final IPropertyService propertyService;

    public UserService(@NotNull final IConnectionService connectionService,
                       @NotNull final IPropertyService propertyService
    ) {
        super(connectionService);
        this.propertyService = propertyService;
    }


    @Override
    public void add(@Nullable UserDTO user) {
        if (user == null) throw new UserNotFoundException();
        @NotNull final SqlSession session = getSqlSession();
        try {
            @NotNull final IUserRepository repository = session.getMapper(IUserRepository.class);
            repository.add(user);
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public void addALl(@Nullable Collection<UserDTO> users) {
        if (users == null || users.isEmpty()) throw new UserNotFoundException();
        @NotNull final SqlSession session = getSqlSession();
        try {
            @NotNull final IUserRepository repository = session.getMapper(IUserRepository.class);
            for (UserDTO user : users)
                repository.add(user);
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public void set(@Nullable Collection<UserDTO> users) {
        if (users == null || users.isEmpty()) throw new UserNotFoundException();
        @NotNull final SqlSession session = getSqlSession();
        try {
            @NotNull final IUserRepository repository = session.getMapper(IUserRepository.class);
            repository.clear();
            for (UserDTO user : users)
                repository.add(user);
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public long getSize() {
        try (@NotNull final SqlSession session = getSqlSession()) {
            @NotNull final IUserRepository repository = session.getMapper(IUserRepository.class);
            return repository.getSize();
        }
    }

    @Nullable
    @Override
    public List<UserDTO> findAll() {
        @Nullable final List<UserDTO> users;
        try (@NotNull final SqlSession session = getSqlSession()) {
            @NotNull final IUserRepository repository = session.getMapper(IUserRepository.class);
            users = repository.findAll();
            if (users == null || users.isEmpty()) return Collections.emptyList();
            return users;
        }
    }

    @Nullable
    @Override
    public UserDTO findById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new TaskIdEmptyException();
        @Nullable final UserDTO user;
        try (@NotNull final SqlSession session = getSqlSession()) {
            @NotNull final IUserRepository repository = session.getMapper(IUserRepository.class);
            return repository.findById(id);
        }
    }

    @Override
    public void clear() {
        @NotNull final SqlSession session = getSqlSession();
        try {
            @NotNull final IUserRepository repository = session.getMapper(IUserRepository.class);
            repository.clear();
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public void removeById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new UserIdEmptyException();
        @NotNull final SqlSession session = getSqlSession();
        try {
            @NotNull final IUserRepository repository = session.getMapper(IUserRepository.class);
            repository.removeById(id);
            session.commit();
        } catch (Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @NotNull
    @Override
    public UserDTO create(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new LoginAlreadyExistException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final UserDTO user = new UserDTO();
        @NotNull final SqlSession session = getSqlSession();
        try {
            @NotNull final IUserRepository repository = session.getMapper(IUserRepository.class);
            user.setLogin(login);
            user.setRole(Role.USUAL);
            @NotNull final String secret = propertyService.getPasswordSecret();
            @NotNull final Integer iteration = propertyService.getPasswordIteration();
            user.setPasswordHash(HashUtil.salt(password, secret, iteration));
            repository.add(user);
            session.commit();
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
        return user;
    }

    @NotNull
    @Override
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        if (isEmailExist(email)) throw new EmailAlreadyExistException();
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new LoginAlreadyExistException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final SqlSession session = getSqlSession();
        @NotNull final UserDTO user = new UserDTO();
        try {
            @NotNull final IUserRepository repository = session.getMapper(IUserRepository.class);
            user.setLogin(login);
            user.setRole(Role.USUAL);
            @NotNull final String secret = propertyService.getPasswordSecret();
            @NotNull final Integer iteration = propertyService.getPasswordIteration();
            user.setPasswordHash(HashUtil.salt(password, secret, iteration));
            user.setEmail(email);
            repository.add(user);
            session.commit();
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
        return user;
    }

    @NotNull
    @Override
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) {
        if (role == null) throw new RoleIsEmptyException();
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new LoginAlreadyExistException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final UserDTO user = new UserDTO();
        @NotNull final SqlSession session = getSqlSession();
        try {
            @NotNull final IUserRepository repository = session.getMapper(IUserRepository.class);
            user.setLogin(login);
            user.setRole(Role.USUAL);
            @NotNull final String secret = propertyService.getPasswordSecret();
            @NotNull final Integer iteration = propertyService.getPasswordIteration();
            user.setPasswordHash(HashUtil.salt(password, secret, iteration));
            user.setRole(role);
            repository.add(user);
            session.commit();
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
        return user;
    }

    @NotNull
    @Override
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email,
            @Nullable final Role role
    ) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        if (isEmailExist(email)) throw new EmailAlreadyExistException();
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new LoginAlreadyExistException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleIsEmptyException();
        @NotNull final UserDTO user = new UserDTO();
        @NotNull final SqlSession session = getSqlSession();
        try {
            @NotNull final IUserRepository repository = session.getMapper(IUserRepository.class);
            user.setLogin(login);
            user.setRole(Role.USUAL);
            @NotNull final String secret = propertyService.getPasswordSecret();
            @NotNull final Integer iteration = propertyService.getPasswordIteration();
            user.setPasswordHash(HashUtil.salt(password, secret, iteration));
            user.setEmail(email);
            user.setRole(role);
            repository.add(user);
            session.commit();
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
        return user;
    }

    @Nullable
    @Override
    public UserDTO findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final UserDTO result;
        try (@NotNull final SqlSession session = getSqlSession()) {
            @NotNull final IUserRepository repository = session.getMapper(IUserRepository.class);
            return repository.findByLogin(login);
        }
    }

    @Nullable
    @Override
    public UserDTO findByEmail(@Nullable String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @Nullable final UserDTO result;
        try (@NotNull final SqlSession session = getSqlSession()) {
            @NotNull final IUserRepository repository = session.getMapper(IUserRepository.class);
            return repository.findByEmail(email);
        }
    }

    @Override
    public void setPassword(@Nullable final String id, @Nullable final String password) {
        if (id == null || id.isEmpty()) throw new UserIdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final SqlSession session = getSqlSession();
        @Nullable final UserDTO result;
        try {
            @NotNull final IUserRepository repository = session.getMapper(IUserRepository.class);
            result = findById(id);
            @NotNull final String secret = propertyService.getPasswordSecret();
            @NotNull final Integer iteration = propertyService.getPasswordIteration();
            @NotNull final String passwordHash = HashUtil.salt(password, secret, iteration);
            @NotNull final UpdateStatementProvider statement = SqlBuilder.update(users).
                    set(UserProvider.passwordHash).equalTo(passwordHash).
                    where(UserProvider.id, isEqualTo(id)).
                    build().render(RenderingStrategy.MYBATIS3);
            repository.setPassword(statement);
            session.commit();
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @NotNull
    @Override
    public UserDTO updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String middleName,
            @Nullable final String lastName) {
        if (id == null || id.isEmpty()) throw new UserIdEmptyException();
        @NotNull final SqlSession session = getSqlSession();
        @Nullable final UserDTO result;
        try {
            @NotNull final IUserRepository repository = session.getMapper(IUserRepository.class);
            result = repository.findById(id);
            if (result == null) throw new UserNotFoundException();
            repository.updateUser(id, firstName, middleName, lastName);
            session.commit();
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
        return result;
    }

    @Override
    public void update(@Nullable final UserDTO user) {
        if (user == null) throw new UserNotFoundException();
        @NotNull final SqlSession session = getSqlSession();
        try {
            @NotNull final IUserRepository repository = session.getMapper(IUserRepository.class);
            @NotNull final UpdateStatementProvider statement = SqlBuilder.update(users).
                    set(login).equalTo(user.getLogin()).
                    set(passwordHash).equalTo(user.getPasswordHash()).
                    set(email).equalTo(user.getEmail()).
                    set(firstName).equalTo(user.getFirstName()).
                    set(middleName).equalTo(user.getMiddleName()).
                    set(lastName).equalTo(user.getLastName()).
                    set(locked).equalTo(user.getLocked()).
                    set(role).equalTo(user.getRole().name()).
                    where(UserProvider.id, isEqualTo(user.getId())).
                    build().render(RenderingStrategy.MYBATIS3);
            repository.update(statement);
            session.commit();
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public void remove(@Nullable final UserDTO user) {
        if (user == null) throw new UserNotFoundException();
        @NotNull final SqlSession session = getSqlSession();
        @Nullable final UserDTO result;
        try {
            @NotNull final IUserRepository repository = session.getMapper(IUserRepository.class);
            @NotNull final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
            @NotNull final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
            result = repository.findById(user.getId());
            if (result == null) throw new UserNotFoundException();
            @NotNull final String userId = result.getId();
            taskRepository.clearForUser(userId);
            projectRepository.clearForUser(userId);
            repository.remove(result);
            session.commit();
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public void removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final SqlSession session = getSqlSession();
        try {
            @NotNull final IUserRepository repository = session.getMapper(IUserRepository.class);
            @Nullable final UserDTO repositoryUser = repository.findByLogin(login);
            if (repositoryUser == null) throw new UserNotFoundException();
            repository.removeByLogin(login);
            session.commit();
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @NotNull
    @Override
    public Boolean isLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        try (@NotNull final SqlSession session = getSqlSession()) {
            @NotNull final IUserRepository repository = session.getMapper(IUserRepository.class);
            return repository.isLoginExist(login);
        }
    }

    @NotNull
    @Override
    public Boolean isEmailExist(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        try (@NotNull final SqlSession session = getSqlSession()) {
            @NotNull final IUserRepository repository = session.getMapper(IUserRepository.class);
            return repository.isEmailExist(email);
        }
    }

    @Override
    public void lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final SqlSession session = getSqlSession();
        try {
            @NotNull final IUserRepository repository = session.getMapper(IUserRepository.class);
            @Nullable final UserDTO repositoryUser = repository.findByLogin(login);
            if (repositoryUser == null) throw new UserNotFoundException();
            repository.lockUserByLogin(login);
            session.commit();
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public void unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final SqlSession session = getSqlSession();
        try {
            @NotNull final IUserRepository repository = session.getMapper(IUserRepository.class);
            @Nullable final UserDTO repositoryUser = repository.findByLogin(login);
            if (repositoryUser == null) throw new UserNotFoundException();
            repository.unlockUserByLogin(login);
            session.commit();
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

}
