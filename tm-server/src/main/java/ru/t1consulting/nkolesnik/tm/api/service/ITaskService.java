package ru.t1consulting.nkolesnik.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.enumerated.Sort;
import ru.t1consulting.nkolesnik.tm.enumerated.Status;
import ru.t1consulting.nkolesnik.tm.dto.model.TaskDTO;

import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public interface ITaskService extends IUserOwnedService<TaskDTO> {

    void add(@Nullable TaskDTO task);

    void add(@Nullable String userId, @Nullable TaskDTO task);

    void add(@Nullable Collection<TaskDTO> tasks);

    void add(@Nullable String userId, @Nullable Collection<TaskDTO> tasks);

    void set(@Nullable Collection<TaskDTO> tasks);

    void set(@Nullable String userId, @Nullable Collection<TaskDTO> tasks);

    @NotNull
    TaskDTO create(@Nullable String userId, @Nullable String name);

    @NotNull
    TaskDTO create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @Nullable
    TaskDTO create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description,
            @Nullable Date dateBegin,
            @Nullable Date dateEnd
    );

    long getSize();

    long getSize(@Nullable String userId);

    @NotNull
    List<TaskDTO> findAll();

    @NotNull
    List<TaskDTO> findAll(@Nullable String userId);

    @NotNull
    List<TaskDTO> findAll(@Nullable Comparator<TaskDTO> comparator);

    @NotNull
    List<TaskDTO> findAll(@Nullable Sort sort);

    @NotNull
    List<TaskDTO> findAll(@Nullable String userId, @Nullable Comparator<TaskDTO> comparator);

    @NotNull
    List<TaskDTO> findAll(@Nullable String userId, @Nullable Sort sort);

    @NotNull
    List<TaskDTO> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

    @Nullable
    TaskDTO findById(@Nullable String id);

    @Nullable
    TaskDTO findById(@Nullable String userId, @Nullable String id);

    boolean existsById(@Nullable String id);

    boolean existsById(@Nullable String userId, @Nullable String id);

    void update(@Nullable TaskDTO task);

    void updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    void changeTaskStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    void clear();

    void clear(@Nullable String userId);

    void remove(@Nullable TaskDTO task);

    void remove(@Nullable String userId, @Nullable TaskDTO task);

    void removeById(@Nullable String id);

    void removeById(@Nullable String userId, @Nullable String id);

    void removeByProjectId(@Nullable String userId, @Nullable String projectId);

}
