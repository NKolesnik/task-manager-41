package ru.t1consulting.nkolesnik.tm.dto.response.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.dto.response.AbstractUserResponse;
import ru.t1consulting.nkolesnik.tm.dto.model.UserDTO;

@NoArgsConstructor
public final class UserUpdateResponse extends AbstractUserResponse {

    public UserUpdateResponse(@Nullable final UserDTO user) {
        super(user);
    }

}

