package ru.t1consulting.nkolesnik.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.api.model.IWBS;
import ru.t1consulting.nkolesnik.tm.enumerated.Status;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tasks")
public final class TaskDTO extends AbstractUserOwnedModelDTO implements IWBS {

    private static final long serialVersionUID = 1;

    @Nullable
    @Column(name = "project_id")
    private String projectId;

}
